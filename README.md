# Fitnes Project README


# Venv
### We use python3.6. Please, install it:

`sudo apt-get install python3.6-dev`

`sudo add-apt-repository ppa:jonathonf/python-3.6`

`sudo apt-get update`

`sudo apt-get install python3.6`

### ...then create our project:

`mkdir crazyfatburn && cd crazyfatburn`

`git clone git@bitbucket.org:ru_fitnessdev/fitness.git .`

`pip install virtualenv`

`virtualenv venv --no-site-packages --distribute -p python3.6`

`source venv/bin/activate`

`pip install -r req.txt`



### Start migrations:

`python manage.py migrate`

`python manage.py migrate --run-syncdb`

`python manage.py createsuperuser`

`python manage.py runserver 8001`

`celery -A fit worker  --app=main.celery:app  --loglevel=info -E`

перейти по ссылке
`http://localhost:8000/api/`


# rabbitmq for celery

`sudo apt-get install rabbitmq-server`


