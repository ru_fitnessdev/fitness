{%- extends 'emails/_layouts/customer.txt' -%}

{%- block EMAIL_BODY %}
Уважаемый {{ user.first_name|e }} {{ user.middle_name|e }}! Вы успешно зарегистрировались на сайтe {{ site_name() }}.

Ваш данные при регистрации:
Логин: {{ user.username|e }}
Пароль: {{ raw_password|e }}
{%- endblock -%}
