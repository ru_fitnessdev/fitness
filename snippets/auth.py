# -*- coding: utf-8 -*-
from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend

from snippets.utils.telephones import prepare_telephone_number

UserModel = get_user_model()


class PhoneModelBackend(ModelBackend):
    """Authenticates against user phone"""
    def authenticate(self, request, username=None, password=None, **kwargs):
        if username is not None:
            # оставляем цифры
            phone_number = prepare_telephone_number(username)

            try:
                user = UserModel._default_manager.get(
                    current_phone__is_deleted=False,
                    current_phone__digits__exact=phone_number
                )
            except UserModel.DoesNotExist:
                # Run the default password hasher once to reduce the timing
                # difference between an existing and a non-existing user (#20760).
                UserModel().set_password(password)
            else:
                if user.check_password(password) and self.user_can_authenticate(user):
                    return user
