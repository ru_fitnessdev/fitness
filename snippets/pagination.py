# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from collections import OrderedDict

from rest_framework.response import Response
from rest_framework import pagination


class NoPagination(pagination.BasePagination):
    """
    Класс пагинации, где мета-данные самой пагинации не выводятся,
    для экономии времкни выполнения
    """
    max_page_size = 100
    page_query_param = 'page'
    page_size = 20
    page_size_query_param = 'page_size'

    def get_page_size(self, request):
        page_size = self.page_size
        try:
            page_size = int(request.query_params[self.page_size_query_param])
            if page_size < 1:
                page_size = self.page_size
            elif page_size > self.max_page_size:
                page_size = self.max_page_size

        except (KeyError, ValueError):
            pass

        return page_size

    def paginate_queryset(self, queryset, request, view=None):
        page_size = self.get_page_size(request)
        if not page_size:
            return None
        try:
            page_number = int(request.query_params.get(self.page_query_param, 1))
        except ValueError:
            page_number = 1

        offset = page_size * (page_number - 1)
        return queryset[offset:offset + page_size]

    def to_html(self):
        # это нам не важно, у нас только REST API
        return ''

    def get_paginated_response(self, data):
        return Response(data)


class RPTPageNumberPagination(pagination.PageNumberPagination):
    page_size_query_param = 'page_size'
    page_size = 100
    max_page_size = 500

    def get_paginated_response(self, data):
        return Response(OrderedDict([
            ('count', self.page.paginator.count),
            ('next', self.get_next_link()),
            ('page', self.page.number),
            ('previous', self.get_previous_link()),
            ('results', data)
        ]))


class NoLimitPagination(RPTPageNumberPagination):
    page_size = max_page_size = 1000000


class NoLimitDirectResultPagination(NoLimitPagination):
    def get_paginated_response(self, data):
        return Response(data)


class Per10Pagination(RPTPageNumberPagination):
    page_size = 10
    max_page_size = 100


class Per20Pagination(RPTPageNumberPagination):
    page_size = 20
    max_page_size = 100


class Per50Pagination(RPTPageNumberPagination):
    page_size = 50
    max_page_size = 250


class Per100Pagination(RPTPageNumberPagination):
    page_size = 100
    max_page_size = 500


class Per500Pagination(RPTPageNumberPagination):
    page_size = 500
    max_page_size = 50000


class Per1000Pagination(RPTPageNumberPagination):
    page_size = 1000
    max_page_size = 5000


class Per5000Pagination(RPTPageNumberPagination):
    page_size = 5000
    max_page_size = 50000


class Per10000Pagination(RPTPageNumberPagination):
    page_size = 10000
    max_page_size = 50000
