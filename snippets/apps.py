# -*- coding: utf-8 -*-
from django.apps import AppConfig as BaseAppConfig
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.contrib.sites.models import Site

class AppConfig(BaseAppConfig):
    name = 'snippets'
    verbose_name = _('Библиотека')


def current_site():
    return Site.objects.get(id=settings.SITE_ID)
