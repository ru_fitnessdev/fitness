# -*- coding: utf-8 -*-
import random
import string


DEFAULT_HASH_SAMPLE = string.ascii_letters + string.digits


def generate_random_string(length=24, sample=DEFAULT_HASH_SAMPLE):
    lst = [random.choice(sample) for n in range(length)]
    return ''.join(lst)


def set_unique_for_model(model, field=None, length=16, keys=None,
                         sample=DEFAULT_HASH_SAMPLE):
    while True:
        key = keys or generate_random_string(length=length, sample=sample)
        if not model.objects.filter(**{field + '__exact': key}).exists():
            break
    return key
