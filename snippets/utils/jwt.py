# -*- coding: utf-8 -*-
from calendar import timegm

from django.conf import settings
from django.utils.encoding import smart_text
from django.utils.translation import gettext as _

import jwt

from snippets.api.exceptions import AuthenticationFailed, AuthenticationExpired, \
    AuthenticationLoginRequired
from snippets.datetime import utcnow
from users.models import User
from users.utils import get_user_auth_token_subject


# срок годности токена
EXPIRATION_KEY = 'exp'

# issuer - кто запрашивает (ID пользователя)
ISS_KEY = 'iss'

# время генерации токена:
ISSUED_AT_KEY = 'iat'

# когда в первый раз был выдан (момент входа)
ORIGINAL_ISSUED_AT_KEY = 'oia'

# Идентификатор входа (соль пользователя).
# Меняется, если меняется пароль или посылается запрос на выход из всех устройств
SUBJECT_KEY = 'sub'


def prepare_payload(user, expiry=settings.JWT_EXPIRATION_DELTA, old_payload=None,
                    subject_getter=get_user_auth_token_subject):
    """Подготавливает данные для токена"""
    payload = {
        EXPIRATION_KEY: timegm((utcnow() + expiry).utctimetuple()),
        ISS_KEY: user.alt_id,
        ISSUED_AT_KEY: timegm(utcnow().utctimetuple()),
        SUBJECT_KEY: subject_getter(user)
    }

    if settings.JWT_ALLOW_REFRESH:
        payload[ORIGINAL_ISSUED_AT_KEY] = old_payload[ORIGINAL_ISSUED_AT_KEY] \
            if old_payload else timegm(utcnow().utctimetuple())

    return payload


def get_user_alt_id_from_payload(payload):
    """Ивлекает ID пользователя из данных токена"""
    return payload.get(ISS_KEY)


def get_subject_from_payload(payload):
    """Ивлекает соль пользователя из данных токена"""
    return payload.get(SUBJECT_KEY)


def encode_payload(payload):
    """Запаковывает данные в токен"""
    return jwt.encode(
        payload,
        settings.JWT_SECRET_KEY,
        settings.JWT_ALGORITHM
    ).decode('utf-8')


def decode_token(token, verify_expired=settings.JWT_VERIFY_EXPIRATION):
    """Распаковывает токен"""
    try:
        payload = jwt.decode(
            token,
            settings.JWT_SECRET_KEY,
            settings.JWT_VERIFY,
            options={
                'verify_exp': verify_expired,
                'verify_iat': settings.JWT_VERIFY_IAT
            },
            leeway=settings.JWT_LEEWAY
        )
    except jwt.DecodeError:
        raise AuthenticationFailed(_('Ошибка декодирования заголовка авторизации.'))

    except jwt.ExpiredSignature:
        raise AuthenticationExpired(_('Заголовок авторизации устарел.'))

    if not check_token_expiration(payload):
        raise AuthenticationLoginRequired(_('Заголовок авторизации устарел. Требуется вход.'))

    return payload


def check_subject_code(user, payload):
    """
    Проверяет sub токена (уникальный ключ, в нашел случае - соль токена пользователя)
    При смене пароля соль меняется (либо меняется вручную по запросу), что позволяет
    аннулировать все старые токены
    """
    return get_user_auth_token_subject(user) == get_subject_from_payload(payload)


def check_token_expiration(payload):
    """
    Проверяет, если токену более JWT_REFRESH_EXPIRATION_DELTA с времени создания, то он бракуется
    """
    if not getattr(settings, 'JWT_REFRESH_EXPIRATION_DELTA'):
        return True

    now = timegm(utcnow().utctimetuple())
    iat = payload.get(ISSUED_AT_KEY)
    return bool(iat) and isinstance(iat, int) \
        and (now - iat < settings.JWT_REFRESH_EXPIRATION_DELTA.total_seconds())


def authenticate_credentials(payload, subject_checker=check_subject_code):
    """
    Returns an active user that matches the payload's user id and email.
    """
    try:
        alt_id = get_user_alt_id_from_payload(payload)

        if alt_id:
            user = User.objects.get(alt_id=alt_id)
        else:
            raise AuthenticationFailed(_('Неправильные данные заголовка авторизации.'))

    except User.DoesNotExist:
        raise AuthenticationFailed(_('Пользователь не найден.'))

    if not user.is_active:
        raise AuthenticationFailed(_('Пользователь не активен.'))

    if not subject_checker(user, payload):
        raise AuthenticationFailed(
            _('Неправильные данные заголовка авторизации, или он устарел.')
        )

    return user


def get_authorization_header(request):
    """Извлекает токен из заголовков запроса (Authorization)"""
    auth = request.META.get('HTTP_AUTHORIZATION', b'')

    if isinstance(auth, str):
        # Work around django test client oddness
        auth = auth.encode('iso-8859-1')

    return auth


def extract_token_from_request(request):
    auth = get_authorization_header(request).split()
    auth_header_prefix = settings.JWT_AUTH_HEADER_PREFIX.lower()

    if not auth or smart_text(auth[0].lower()) != auth_header_prefix:
        raise AuthenticationFailed(_('Неправильный заголовок авторизации.'))

    if len(auth) == 1:
        raise AuthenticationFailed(
            _('Неправильный заголовок авторизации. Данные не представлены.')
        )
    elif len(auth) > 2:
        raise AuthenticationFailed(
            _('Неправильный заголовок авторизации. Данные не должны содержать пробел.')
        )
    return auth[1]
