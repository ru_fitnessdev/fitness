# -*- coding: utf-8 -*-
import re


def prepare_telephone_number(phone_number):
    phone_number = re.sub(r'[^0-9]+', '', phone_number)
    # специально для России
    if phone_number.startswith('8'):
        phone_number = '7' + phone_number[1:]

    return phone_number
