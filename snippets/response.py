# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponse
from rest_framework.response import Response


class HttpResponseUnauthorized(HttpResponse):
    def __init__(self, *args, **kwargs):
        self.status_code = 401
        super(HttpResponseUnauthorized, self).__init__(*args, **kwargs)


def response(message=None, status_code=200, data=[]):
    result = {
        'status_code': status_code,
        'detail': data
    }
    if message is not None:
        result['text'] = message
    return Response(result, status=status_code)
