# coding: utf-8
from django.db.models import Manager


class IsActiveManager(Manager):
    def get_queryset(self):
        return super(IsActiveManager, self).get_queryset().\
            filter(is_active=True)


class DeletableManager(Manager):
    def get_queryset(self):
        return super(DeletableManager, self).get_queryset().\
            filter(is_deleted=False)
