# -*- coding: utf-8 -*-
import uuid
import os
from django.db import models
from django.db.models import ForeignKey
from django.db.models import Manager
from django.db.models import QuerySet
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from easy_thumbnails.fields import ThumbnailerImageField
from snippets.models.enumerates import StatusEnum


CREATED_VERBOSE = _('Создано')
UPDATED_VERBOSE = _('Обновлено')
UTIL_FIELDS = ('id', 'created', 'updated', 'ordering', 'status')


class IsActiveManager(models.Manager):
    def get_queryset(self):
        return super(IsActiveManager, self).get_queryset().\
            filter(is_active=True)


class DeletableManager(models.Manager):
    def get_queryset(self):
        return super(DeletableManager, self).get_queryset().\
            filter(is_deleted=False)


class BaseIsActiveModel(models.Model):
    is_active = models.BooleanField(_('Активен'), default=True)

    objects = models.Manager()
    active_objects = IsActiveManager()

    class Meta:
        abstract = True


class VirtualDeletableMixin(models.Model):
    """Миксин позволяет сделать удаление модели виртуальным"""
    is_deleted = models.BooleanField('Удален', default=False)
    objects = Manager()
    existing_objects = DeletableManager()

    def delete(self, *args, **kwargs):
        self.is_deleted = True
        self.save()

    def restore(self):
        self.is_deleted = False
        self.save()

    class Meta:
        abstract = True


class BaseQuerySet(QuerySet):
    def published(self):
        return self.filter(status__exact=StatusEnum.PUBLIC)

    def hidden(self):
        return self.filter(status__exact=StatusEnum.HIDDEN)

    def draft(self):
        return self.filter(status__exact=StatusEnum.DRAFT)


BaseManager = Manager.from_queryset(BaseQuerySet)
BaseManager.use_for_related_fields = True


class BasicModel(models.Model):
    objects = Manager()

    @staticmethod
    def collect_fields():
        return None

    def __repr__(self):
        return self.__str__()

    class Meta:
        abstract = True


class ContentTypeModel(models.Model):
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:
        abstract = True


class CreatedModel(models.Model):
    created = models.DateTimeField(
        CREATED_VERBOSE, auto_now_add=True, db_index=True
    )

    class Meta:
        abstract = True


class LastModModel(CreatedModel):
    """Base model for all models with created / updated fields"""
    updated = models.DateTimeField(UPDATED_VERBOSE, auto_now=True)

    class Meta:
        abstract = True


class StatusOrderingModel(models.Model):
    """Base model for all objects"""
    ordering = models.IntegerField(_('Порядок'), default=0, db_index=True)
    status = models.SmallIntegerField(
        _('Статус'),
        default=StatusEnum.PUBLIC,
        choices=StatusEnum.get_choices()
    )

    def collect_fields(self):
        fields = []
        has_status_ordering = False
        for field in self._meta.fields:
            if field.attname == 'status':
                has_status_ordering = True
            if field.attname not in UTIL_FIELDS:
                if isinstance(field, ForeignKey):
                    fields.append(field.attname.replace('_id', ''))
                else:
                    fields.append(field.attname)
        return fields + (['status', 'ordering'] if has_status_ordering else [])

    class Meta:
        abstract = True


class BaseModel(BasicModel, LastModModel):
    objects = BaseManager()

    class Meta:
        abstract = True


class BaseDictionaryModel(VirtualDeletableMixin, BaseModel,
                          StatusOrderingModel):
    title = models.CharField(_('Заголовок'), max_length=400, unique=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.title


class BaseTextModel(BaseModel):
    text = models.TextField(_('Текст'), null=True, blank=True)

    class Meta:
        abstract = True


class BasePostModel(BaseTextModel):
    title = models.CharField(_('Заголовок'), max_length=224)

    class Meta:
        abstract = True

    def __str__(self):
        return self.title


def get_photo_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = '%s.%s' % (uuid.uuid4(), ext)
    return os.path.join(
        'images', 'user', filename[:2], filename[2:4], filename
    )


class BasePhotoModel(BaseModel, StatusOrderingModel):
    photo = ThumbnailerImageField(
        _('Фотография'), upload_to=get_photo_path, blank=True
    )

    class Meta:
        abstract = True


class RelatedPhoto(BasePhotoModel, ContentTypeModel):
    class Meta:
        abstract = True
