# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _

from snippets.api.response import error_response


def public(function):
    function.is_public_http_method = True
    return function


def authenticated_should_not_pass(api_method, http_status=403):
    def wrapper(handler, request, *args, **kwargs):
        # запрещаем для уже вошедших
        if request.user.is_authenticated():
            return error_response(
                _('Вы уже вошли в систему, регистрация невозможна'),
                status=http_status
            )
        return api_method(handler, request, *args, **kwargs)
    return wrapper
