# -*- coding: utf-8 -*-
import math

from django.db.models import QuerySet


class BasePagination(object):
    """Базовый пагинатор списков"""
    def __init__(self, request, view):
        self.page = self.get_page(request, view)
        self.page_size = self.get_page_size(request, view)
        self.count = None

    @staticmethod
    def extract_page(request, view):
        return request.GET.get(view.page_param, None)

    def get_page(self, request, view):
        page = self.extract_page(request, view)
        try:
            page = int(page)
        except (ValueError, TypeError):
            page = 1

        if page < 1:
            page = 1

        return page

    @staticmethod
    def get_page_size(request, view):
        page_size = view.page_size
        try:
            page_size = int(request.GET[view.page_size_param])
            if page_size < 1:
                page_size = view.page_size
            elif page_size > view.max_page_size:
                page_size = view.max_page_size

        except (KeyError, ValueError):
            pass

        return page_size

    @staticmethod
    def get_count(objects):
        return objects.count() if isinstance(objects, QuerySet) else len(objects)

    def paginate(self, objects, total_count=None):
        self.count = self.get_count(objects) if total_count is None else total_count

        bottom = (self.page - 1) * self.page_size
        top = self.page * self.page_size
        return objects[bottom:top]

    def response_result(self, data):
        raise NotImplementedError


class Pagination(BasePagination):
    """Обычный пагинатор"""
    def response_result(self, data):
        return {
            'num_results': self.count,
            'total_pages': math.ceil(self.count / self.page_size),
            'page': self.page,
            'objects': data
        }


class DirectPagination(BasePagination):
    """Пагинатор, который отдает результат сразу (общее количество извлекается в отдельном API"""
    @staticmethod
    def get_count(objects):
        return None

    def response_result(self, data):
        return data
