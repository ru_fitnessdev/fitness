# -*- coding: utf-8 -*-
from django.http import HttpResponse, JsonResponse


class Response(JsonResponse):
    pass


class HttpResponseUnauthorized(HttpResponse):
    def __init__(self, *args, **kwargs):
        self.status_code = 401
        super(HttpResponseUnauthorized, self).__init__(*args, **kwargs)


def error_response(message=None, status=400, code=None):
    result = {
        'status': 'error'
    }
    if message is not None:
        result['detail'] = message

    if code:
        result['code'] = code
    return Response(result, status=status)


def success_response(message=None, status=200):
    result = {
        'status': 'ok'
    }
    if message is not None:
        result['detail'] = message
    return Response(result, status=status)


def validation_error_response(errors, status=400, code=None):
    return error_response(message={'errors': errors}, status=status, code=code)


def not_authenticated_response(message=None, authentication_header=None):
    response = error_response(message=message, status=401)
    if authentication_header is not None:
        response['WWW-Authenticate'] = authentication_header

    return response
