# -*- coding: utf-8 -*-

default_error_messages = {
    'required': 'Обязательное поле.',
    'type': 'Введен неправильный тип данных.',  # used by Unmarshaller
    'null': 'Поле не может быть пустым (null)',
    'validator_failed': 'Неправильное значение'
}
