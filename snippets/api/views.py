# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import gc

from rest_framework.generics import ListAPIView, UpdateAPIView
from rest_framework.views import APIView
import six

from snippets.api.exceptions import APIValidationError
from snippets.utils.date import parse_date


class GarbageCollectViewMixin(object):
    def initial(self, request, *args, **kwargs):
        gc.collect()
        return super(GarbageCollectViewMixin, self).initial(request, *args, **kwargs)


class BaseJSONViewMixin(object):
    def finalize_response(self, request, response, *args, **kwargs):
        response = super(BaseJSONViewMixin, self).finalize_response(
            request, response, *args, **kwargs
        )

        if hasattr(response, 'accepted_media_type') \
                and response.accepted_media_type.startswith('application/json'):
            response.accepted_media_type = 'application/json'

        return response


class BaseAPIView(BaseJSONViewMixin, APIView):
    pass


class BaseListAPIView(BaseJSONViewMixin, ListAPIView):
    pass


class BaseUpdateAPIView(BaseJSONViewMixin, UpdateAPIView):
    pass


class BaseValidatedParamsAPIView(BaseAPIView):
    required_params = None
    available_params = None

    def clean_params(self, data, request):
        for k, v in six.iteritems(data):
            validator = getattr(self, 'get_' + k, None)
            if validator and callable(validator):
                data[k] = validator(v, request)
            # иначе считаем, что валидация параметра не требуется
        return data

    def validate_get(self, request, data):
        result = dict()

        if self.available_params:
            for k, v in six.iteritems(data):
                if k not in self.available_params:
                    continue
                result[k] = v

        if self.required_params:
            for k in self.required_params:
                if k not in result:
                    raise APIValidationError('Пропущен обязательный параметр %s' % k)

        return self.clean_params(result, request)

    def validate(self, request, data):
        result, error = None, None

        validator = getattr(self, 'validate_' + request.method.lower(), None)
        if validator and callable(validator):
            try:
                result = validator(request, data)
            except APIValidationError as e:
                return result, e.message

        return result, error

    @staticmethod
    def get_date(date_param, value, request):
        parsed_date = parse_date(value)
        if parsed_date:
            return parsed_date
        raise APIValidationError(
            'Неправильный формат параметра %s (должен быть "ГГГГ-ММ-ДД")' % date_param
        )

    @classmethod
    def get_date_from(cls, value, request):
        return cls.get_date('date_from', value, request)

    @classmethod
    def get_date_to(cls, value, request):
        return cls.get_date('date_to', value, request)
