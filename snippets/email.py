# -*- coding: utf-8 -*-
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string

from dicts.db_config import db_vars


def get_default_from_email(force_system=False):
    if force_system:
        return settings.DEFAULT_FROM_EMAIL

    email = db_vars.get('DEFAULT_FROM_EMAIL', None)
    return email if email else settings.DEFAULT_FROM_EMAIL


def send_templated_email(template, emails, subject, params=None,
                         extra_headers=None, from_email=None):
    if from_email is None:
        from_email = get_default_from_email()

    message_html = render_to_string(
        'emails/%s/%s.html' % (template, template), params, using='jinja2'
    )
    message_txt = render_to_string(
        'emails/%s/%s.txt' % (template, template), params, using='jinja2'
    )

    msg = EmailMultiAlternatives(subject, message_txt, from_email, emails,
                                 headers=extra_headers)
    msg.attach_alternative(message_html, 'text/html')
    msg.send()
