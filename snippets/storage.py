# -*- coding: utf-8 -*-
import os
from django.core.files.storage import FileSystemStorage
from django.conf import settings


class OverwriteStorage(FileSystemStorage):

    def delete_image(self, name, folder):
        if self.exists(name):
            os.remove(os.path.join(settings.MEDIA_ROOT, 'images', folder, name))


class UserAvatarStorage(OverwriteStorage):
    def get_available_name(self, name, max_length=None):
        if self.exists(name):
            self.delete_image(name, 'user_avatars')
        return name

class UserPhotoStorage(OverwriteStorage):
    def get_available_name(self, name, max_length=None):
        if self.exists(name):
            self.delete_image(name, 'user_photos')
        return name
