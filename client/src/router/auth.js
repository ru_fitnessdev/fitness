import AuthLoginPage from '../pages/auth/login/login.vue'
import AuthRegisterPage from '../pages/auth/register/register.vue'

export default [
  {
    path: '/auth/login/',
    name: 'auth.login',
    component: AuthLoginPage,
    props: (route) => ({ nextUrl: 'next' in route.query ? route.query.next : null })
  },
  {
    path: '/auth/register/',
    name: 'auth.register',
    component: AuthRegisterPage,
    props: (route) => ({ nextUrl: 'next' in route.query ? route.query.next : null })
  }
]
