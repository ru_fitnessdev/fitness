import { routes } from './'

export const getRouteByName = (name) => {
  let route = null
  Object.keys(routes).some(path => {
    if (routes[path].name === name) {
      route = routes[path]
      return true
    }
  })
  return route
}
