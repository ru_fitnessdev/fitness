import * as _ from 'lodash'
import Vue from 'vue'
import Router from 'vue-router'

import accountRoutes from './account'
import authRoutes from './auth'
import errorRoutes from './error'
import publicRoutes from './public'

Vue.use(Router)

export const routes = _.concat(
  publicRoutes,
  accountRoutes,
  authRoutes,
  errorRoutes
)

const appRouter = new Router({
  mode: 'history',
  routes
})

export default appRouter
