import HomepagePage from '../pages/public/homepage/homepage.vue'

export default [
  {
    path: '/',
    name: 'homepage',
    component: HomepagePage
  }
]
