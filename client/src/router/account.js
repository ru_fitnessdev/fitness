import AccountDashboard from '../pages/account/dashboard.vue'
import AccountPage from '../pages/account/account.vue'
import AccountSettings from '../pages/account/settings/settings.vue'

export default [
  {
    path: '/account/',
    name: 'account',
    component: AccountPage,
    children: [
      {
        path: 'dashboard/',
        name: 'account.dashboard',
        component: AccountDashboard
      },
      {
        path: 'settings/',
        name: 'account.settings',
        component: AccountSettings
      }
    ]
  }
]
