import NotFoundPage from '../pages/errors/not-found.vue'

export default [
  {
    path: '*',
    name: 'e404',
    component: NotFoundPage
  }
]
