export const getToken = () => {
  return localStorage.token || null
}

export const setToken = (token) => {
  if (process.SERVER_BUILD) {
    return
  }

  localStorage.setItem('token', token)
}

export const unsetToken = () => {
  if (process.SERVER_BUILD) {
    return
  }

  localStorage.removeItem('token')
}
