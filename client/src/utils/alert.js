import * as _ from 'lodash'
import eventHub from '../utils/eventHub'

export const mdAlert = (msg, body) => {
  body = typeof (body) === 'undefined' ? '' : body

  eventHub.$emit('global:alert', {
    title: msg,
    content: `<pre>${body}</pre>`
  })
}

export const errorResponseAlert = (response, msg) => {
  if (!msg) {
    msg = 'Ошибка'
  }

  console.log(`${msg}:`)
  console.log(response.body)

  response.json().then((data) => {
    let detail = data.detail

    if ('errors' in detail) {
      let errors = detail.errors
      detail = []
      _.forOwn(errors, (value, key) => {
        if (key === '__all__') {
          detail.push(value)
        } else {
          detail.push(`<b>${key}</b>: ${value}`)
        }
      })
      detail = detail.join(';\n')
    }

    eventHub.$emit('global:alert', {
      title: msg,
      content: `<pre>${detail}</pre>`
    })
  }, () => {
    eventHub.$emit('global:alert', {
      title: msg,
      content: `<pre>${response.body}</pre>`
    })
  })
}
