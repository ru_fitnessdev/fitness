export default {
  profile () { return `auth/profile/` },
  passwordChange () { return `auth/password-change/` }
}
