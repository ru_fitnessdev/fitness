export default {
  login () { return `auth/api-token-auth/` },
  register () { return `auth/register/` },
  restoreRequest () { return `auth/password-recover/` },
  tokenRefresh () { return `auth/api-token-refresh/` }
}
