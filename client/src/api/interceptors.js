import * as _ from 'lodash'
import Vue from 'vue'

import API, { apiUrl } from './'
// import eventHub from '../utils/eventHub'
import { getToken } from '../utils/auth'
import { mdAlert } from '../utils/alert'
import vueApp from '../main'

export const tokenExcludes = {
  POST: [
    API.auth.login(),
    API.auth.register(),
    API.auth.restoreRequest()
  ]
}

export const getAuthorizationHeaders = () => {
  let token = getToken()
  return {
    'Authorization': `JWT ${token}`
  }
}

const onRequestAttachToken = (request, next) => {
  let excludeToken = false
  if (request.method in tokenExcludes) {
    let url = request.url.replace(apiUrl, '')
    if (_.indexOf(tokenExcludes[request.method], url) >= 0) {
      excludeToken = true
    }
  }

  if (!excludeToken) {
    let headers = getAuthorizationHeaders()
    _.forOwn(headers, (value, key) => {
      request.headers.set(key, value)
    })
  }

  next()
}

const onRequestRefreshToken = (request, next) => {
  // var original = request

  next(response => {
    if (response.status === 401) {
      let url = API.auth.tokenRefresh()
      return vueApp.$http.post(url, { token: getToken() }).then((response) => {
      }).catch(() => {
        // return eventHub.$emit('global:rejectAuth')
      })
    } else if (response.status === 403) {
      // return eventHub.$emit('global:rejectAuth')
    } else if (response.status === 502 || response.status === 504) {
      return mdAlert('Сервер временно недоступен.')
    }
  })
}

export const registerInterceptors = () => {
  Vue.http.interceptors.push(
    onRequestAttachToken,
    onRequestRefreshToken
  )
}
