import AuthAPI from './auth'
import UserAPI from './user'

export const apiUrl = '/api'

export default {
  auth: AuthAPI,
  user: UserAPI
}
