import * as _ from 'lodash'

export default {
  install (Vue) {
    Vue.mixin({
      computed: {
        _ () {
          return _
        }
      }
    })

    Vue.prototype._ = _
  }
}
