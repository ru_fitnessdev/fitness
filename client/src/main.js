import Meta from 'vue-meta'
import ru from 'vee-validate/dist/locale/ru'
import VeeValidate, { Validator } from 'vee-validate'
import Vue from 'vue'
import VueMaterial from 'vue-material'
import VueResource from 'vue-resource'
import App from './app.vue'
import { apiUrl } from './api'
import lodash from './plugins/lodash'
import router from './router'
import store from './store'
import registerComponents from './register'
import { registerInterceptors } from './api/interceptors'

Vue.use(lodash)
Vue.use(Meta, { keyName: 'metaInfo' })
Vue.use(VueMaterial)

// vue-resource
let httpSettings = {
  root: apiUrl
}

Vue.use(VueResource)

Vue.http.options = httpSettings
registerInterceptors()

// vee-validate
Validator.addLocale(ru)
Vue.use(VeeValidate, { locale: 'ru' })

Vue.config.productionTip = false
Vue.material.registerTheme('default', {
  primary: 'indigo',
  accent: 'pink',
  warn: 'deep-orange',
  background: 'grey'
})

registerComponents()

// асинхронно грузим информацию о пользователе при старте, promise запускает App
Vue.component('app', (resolve, reject) => {
  resolve(App)
})

/* eslint-disable no-new */
const vueApp = new Vue({
  el: '#app',
  router,
  store,
  http: httpSettings,
  template: '<app/>'
})

let context = require.context('vue-material/src', true, /\.scss/)
context.keys().forEach((key) => {
  context(key)
})

require('./styles/index.styl')

if (window) {
  window.vue = vueApp
}

export default vueApp
