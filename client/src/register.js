import Vue from 'vue'

import AlertGlobal from './components/global/alert-global.vue'
import FooterOnlyForm from './components/only-form/footer-only-form.vue'
import FooterPublic from './components/public/footer-public.vue'
import HeaderOnlyForm from './components/only-form/header-only-form.vue'
import HeaderPublic from './components/public/header-public.vue'
import LayoutAccount from './components/layouts/account.vue'
import LayoutOnlyForm from './components/layouts/only-form.vue'
import LayoutPublic from './components/layouts/public.vue'

export default () => {
  Vue.component('alert-global', AlertGlobal)
  Vue.component('footer-onlyform', FooterOnlyForm)
  Vue.component('footer-public', FooterPublic)
  Vue.component('header-onlyform', HeaderOnlyForm)
  Vue.component('header-public', HeaderPublic)
  Vue.component('layout-account', LayoutAccount)
  Vue.component('layout-onlyform', LayoutOnlyForm)
  Vue.component('layout-public', LayoutPublic)
}
