import Vue from 'vue'
import Vuex from 'vuex'

import getters from './getters'
import { getToken } from '../utils/auth'
import modules from './modules'
import mutations from './mutations'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token: getToken(),
    user: null
  },
  mutations: mutations,
  getters: getters,
  modules: modules
})
