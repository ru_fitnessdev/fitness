import Vue from 'vue'

import API from '../../api'
import { errorResponseAlert } from '../../utils/alert'
import vueApp from '../../main'

export const login = ({ commit }, data) => {
  let url = API.auth.login()

  return new Vue.Promise((resolve, reject) => {
    vueApp.$http.post(url, data).then(response => {
      commit('SET_USER', response.body, {root: true})
      resolve(response)
    }, (response) => {
      commit('SET_USER_FAIL', null, {root: true})
      errorResponseAlert(response, 'Ошибка при входе в систему')
      reject(response)
    })
  })
}

export const register = ({ commit }, data) => {
  let url = API.auth.register()

  return new Vue.Promise((resolve, reject) => {
    vueApp.$http.post(url, data).then((response) => {
      commit('SET_USER', response.body, {root: true})
      resolve(response)
    }, (response) => {
      commit('SET_USER_FAIL', null, {root: true})
      errorResponseAlert(response, 'Ошибка при регистрации')
      reject(response)
    })
  })
}

export const rejectAuth = ({ commit }) => {
  commit('SET_USER_FAIL', null, {root: true})
}

export default {
  namespaced: true,
  state: {},
  mutations: {},
  actions: {
    login,
    rejectAuth,
    register
  }
}
