import AuthModule from './auth'
import UserModule from './user'

export default {
  auth: AuthModule,
  user: UserModule
}
