import Vue from 'vue'

import API from '../../api'
import { errorResponseAlert } from '../../utils/alert'

export const passwordChange = (state, data) => {
  return new Vue.Promise((resolve, reject) => {
    let url = API.user.passwordChange()
    Vue.http.put(url, data).then(response => {
      resolve(response)
    }, (response) => {
      errorResponseAlert(response, 'Ошибка при изменении пароля')
      reject(response)
    })
  })
}

export const profile = ({commit, state}, data) => {
  return new Vue.Promise((resolve, reject) => {
    let url = API.user.profile()
    Vue.http.get(url).then(response => {
      // commit('SET_USER', {user: response.body}, {root: true})
      console.log(response.body)
      resolve(response)
    }, (response) => {
      commit('SET_USER_FAIL', null, {root: true})
      errorResponseAlert(response, 'Ошибка при изменении личных данных')
      reject(response)
    })
  })
}

export const updateProfile = ({commit}, data) => {
  return new Vue.Promise((resolve, reject) => {
    let url = API.user.profile()
    Vue.http.post(url, data).then(response => {
      commit('SET_USER', {user: response.body}, {root: true})
      resolve(response)
    }, (response) => {
      commit('SET_USER_FAIL', null, {root: true})
      errorResponseAlert(response, 'Ошибка при изменении личных данных')
      reject(response)
    })
  })
}

export default {
  namespaced: true,
  state: {
  },
  mutations: {},
  actions: {
    profile,
    passwordChange
  }
}
