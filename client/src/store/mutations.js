import { setToken, unsetToken } from '../utils/auth'

export default {
  SET_USER (state, payload) {
    state.user = payload.user || null

    if ('token' in payload) {
      state.token = payload.token
      setToken(state.token)
    }
  },

  SET_USER_FAIL (state) {
    state.user = null
    unsetToken()
  }
}
