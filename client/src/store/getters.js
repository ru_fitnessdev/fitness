export default {
  currentUserName: (state) => {
    return state.user ? state.user.first_name || state.user.username || '' : ''
  },
  isUser: (state) => {
    return state.user ? (state.user.role === 'user') : false
  },
  isAuthenticated: (state) => {
    return !!state.token
  }
}
