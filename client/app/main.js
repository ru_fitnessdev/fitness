import Vue from 'vue'
import axios from 'axios'
import App from './App.vue'
import router from './router'
import store from './store'
import Vuetify from 'vuetify'
import Snackbar from './components/snackbar'
import Dialog from './components/dialog'
import Private from './views/layout-private'
import Public from './views/layout-public'
// import 'vuetify/dist/vuetify.min.css'
import { EventBus } from './bus'
// import ru from 'vee-validate/dist/locale/ru'
import VeeValidate, { Validator } from 'vee-validate'
import ElementUI from 'element-ui'
import lang from 'element-ui/lib/locale/lang/ru-RU'
import locale from 'element-ui/lib/locale'
import '../theme/index.css'
import VueQuillEditor from 'vue-quill-editor'

Vue.use(VueQuillEditor)

Vue.use(ElementUI)

locale.use(lang)

require('./styles/main.styl')

// Vue.use(Vuetify)
/*
Validator.addLocale(ru);

Vue.use(VeeValidate, {
    errorBagName: 'vErrors',
    locale: 'ru'
})
*/
Vue.component('layout-private', Private)
Vue.component('layout-public', Public)

const vueApp = new Vue({
  el: '#app',
  template: '<App/>',
  router,
  store,
  components: { App }
})
