import Vue from 'vue'
import VueHead from 'vue-head'
import VueRouter from 'vue-router'
import ls from 'local-storage'

import Home from '../views/private/home.vue'
import Settings from '../views/private/settings.vue'
import Wall from '../views/private/wall.vue'

import Index from '../views/public/index.vue'
import AuthEmailConfirm from '../views/public/auth_emailconfirm.vue'
import AuthRegister from '../views/public/auth_register.vue'
import AuthPasswordRestore from '../views/public/auth_password.vue'
import AuthLogin from '../views/public/auth_login.vue'
import AuthPasswordNew from '../views/public/auth_password_new.vue'

Vue.use(VueHead)
Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  routes: [
    {path: '/', name: 'index', component: Index, title: 'Главная'},
    {path: '/app', name: 'main', component: Home, title: 'Сервис', children:[
        {path: 'wall', name: 'wall', component: Wall, title: 'Стена'},
        {path: 'settings', name: 'settings', component: Settings, title:'Настройки'}
    ]},
    {path: '/login', name: 'auth.login', component: AuthLogin},
    {path: '/password-restore', name: 'auth.password-restore', component: AuthPasswordRestore},
    {path: '/password-new/:token', name: 'auth.password-new', component: AuthPasswordNew},
    {path: '/email-confirm', name: 'auth.email-confirm', component: AuthEmailConfirm},
    {path: '/register', name: 'auth.register', component: AuthRegister}
  ]
})
export default router
