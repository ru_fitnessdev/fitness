import ls from 'local-storage'
import {EventBus} from '@app/bus'

export const localStorageMixin = {
  methods: {
    ls_pushToken (token) {
      ls.set('jwt-token', token)
    },

    ls_pullToken () {
      return ls.get('jwt-token')
    },

    ls_clear () {
      return ls.clear()
    }
  }
}

export const utilsMixin = {
  methods: {
    selectElement (selector) {
      return document.querySelector(`${selector}`)
    },

    focusElement (selector) {
      const element = this.selectElement(selector)
      element.focus()
    }
  }
}

export const errorResponseNotice = function(response) {
  const h = EventBus.$createElement;
  let data = response.data
  let errors = data.detail
  let out = h('div',
    _.map(errors, (value, key) => {
         return h('p', [
            h('b', null, key),
            h('span', null, value),
          ])
    })
  )
  EventBus.$notify.error({
    title: data.text,
    message: out,
    duration: 3000
  });
}
