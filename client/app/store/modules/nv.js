import Vue from 'vue'
import moment from 'moment'

import api from '../api'

const state = {
}

const actions = {
}

const mutations = {
}

const getters = {
}

export default {
  state,
  actions,
  mutations,
  getters
}
