import ls from 'local-storage'
import HTTP from '../api';
import {EventBus} from '@app/bus'
import vueApp from '@app/main'
import {errorResponseNotice} from '@app/mixins'
import {getToken} from '@app/store/ls'

const state = {
    user: null,
    token: getToken(),
    errors:[],
    resetPassword: null,
    resetPasswordDate: null

}

const actions = {

  TOKEN_VERIFY: ({commit, state}, data) => {
      return new Promise((resolve, reject) => {
        if(!state.resetPassword){
          HTTP.post(`auth/api-token-verify/`, data).then((response) => {
                commit('SET_USER', response.data)
                resolve(true)
          }, (response) => {
              commit('SET_USER_FAIL', null, {root: true})
          })
        }
      }) 
  },

  RECOVER_PASSWORD_REQUEST: ({commit, state}, data) => {
      return new Promise((resolve, reject) => {
        HTTP.post(`auth/password-recover/`, data).then((response) => {
          EventBus.$message({
            message: response.data.text,
            type: 'success',
            duration:5000
          });
          resolve(response)
        }, (error) => {
          reject(error)
        })
      })
  },

  CHANGE_PASSWORD: ({commit}, data) => {
      return new Promise((resolve, reject) => {
        HTTP.post(`auth/password-change/`, data).then((response) => {
          commit('SET_RESET_PASSWORD', {resetPassword: false})
          EventBus.$message({
            message: response.data.text,
            type: 'success',
            duration:2000
          });
          resolve(response)
        }, (error) => {
          errorResponseNotice(error.response)
          reject(error)
        })
      })
  },

  GET_TOKENS_PASSWORD: ({commit}, data) => {
      commit('SET_RESET_PASSWORD', {resetPassword: true})
      return new Promise((resolve, reject) => {
        HTTP.get(`auth/password-new/${data.token}`, data).then((response) => {
          commit('SET_USER', response.data.detail)
          resolve(response)
        }, (error) => {
            errorResponseNotice(error.response)
            reject(error)
        })
      })
  },


  LOG_IN_USER: ({commit}, data) => {
      return new Promise((resolve, reject) => {
        HTTP.post(`auth/api-token-auth/`, data).then((response) => {
          commit('SET_USER', response.data)
          EventBus.$message({
            message: 'Успешный вход в систему',
            type: 'success',
            duration:2000
          });
          resolve(response)
        }, (error) => {
          commit('SET_USER_FAIL', null)
          reject(error)
        })
      })
  },

  REGISTER_USER: ({commit}, data) => {
      return new Promise((resolve, reject) => {
          HTTP.put(`auth/register/`, data).then((response) => {
              commit('SET_USER', response.data.detail)
              EventBus.$message({
                message: 'Регистрация завершена. Вы можете войти в систему',
                duration: 4000
              });
              resolve(response)
          }, (error) => {
            commit('SET_USER_FAIL', null)
            errorResponseNotice(error.response)
            reject(error)
          })
      })
  },

  SAVE_USER_PROFILE: ({commit}, data) => {
      return new Promise((resolve, reject) => {
          HTTP.put(`auth/profile/`, data).then((response) => {
             commit('SET_USER', response.data.detail)
             console.log(response.data)
              EventBus.$message({
                message: response.data.text,
                type: 'success',
                duration:2000
              });
              resolve(response)
          }, (error) => {
            errorResponseNotice(error.response)
            reject(error)
          })
      })
  },

  LOG_OUT_USER: ({ commit }, data) => {
      return new Promise((resolve) => {
        commit('LOGOUT_USER')
        resolve({name: 'auth.login'})
      })
  },

   SET_IMAGES: ({ commit }, data) => {
    commit('SET_USER_IMAGES', data)
   }
}



const mutations = {
  SET_RESET_PASSWORD_TIME (state, payload) {
    state.resetPasswordDate = payload.time
    ls.set("resetPasswordDate",  state.resetPasswordDate)
  },
  SET_USER (state, payload) {
    state.user = payload.user || null
    if ('token' in payload) {
      state.token = payload.token
      ls.set("jwt-token", payload.token)
    }
  },
  SET_USER_IMAGES (state, payload) {
    state.user.photo = payload.user.photo
    state.user.avatar = payload.user.avatar
  },
  SET_USER_FAIL (state) {
    state.user = null
    state.token = null
    ls.clear()
  },

  LOGOUT_USER (state) {
    state.user = null
    state.token = null
    ls.clear()
  },

  SET_RESET_PASSWORD (state, payload) {
    state.resetPassword = payload.resetPassword
  }
}


const getters = {
  token: (state) =>  {
    return state.token || getToken()
  },
  user: (state) =>  {
    return state.user
  },
  full_username: (state) =>  {
    if(state.user){
      return  state.user.first_name +" "+ state.user.last_name
    }
  },
  image: (state) => {
    if(state.user){
      return {
        avatar: state.user.avatar || undefined,
        photo: state.user.photo || undefined
      }
    }
  },
 is_reset_password: (state) => {
    return state.resetPassword
  },
  reset_time: (state) => {
    return state.resetPasswordDate || ls.get("resetPasswordDate")
  }
}

export default {
    state,
    actions,
    mutations,
    getters
}


