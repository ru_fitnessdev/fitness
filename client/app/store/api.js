import axios from 'axios';
import ls from 'local-storage'
import store from '@app/store'


let token = store.getters.token

let HTTP = axios.create({
  baseURL: `/api/`,
  headers: {
    Authorization: `JWT ${token}`
  }
})



export default HTTP