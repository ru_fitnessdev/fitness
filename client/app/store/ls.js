export const getToken = () => {
  return JSON.parse(localStorage.getItem("jwt-token")) || null
}

export const setToken = (token) => {
  if (process.SERVER_BUILD) {
    return
  }

  localStorage.setItem('jwt-token', token)
}

export const unsetToken = () => {
  if (process.SERVER_BUILD) {
    return
  }

  localStorage.removeItem('jwt-token')
}
