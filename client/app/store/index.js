import Vue from 'vue'
import Vuex from 'vuex'

import auth from './modules/auth'
import nv from './modules/nv'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    auth,
    nv
  }
})

export default store
