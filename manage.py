#!/usr/bin/env python3.6
import os
import sys
# import locale
from django.core.management import execute_from_command_line
try:
    from main.settings import local as settings
except ImportError as ex:
    try:
        from main.settings import settings as settings
    except ImportError as ex:
        sys.stderr.write("Error: %s.)\n" % repr(ex))
        sys.exit(1)

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", settings)
    # locale.setlocale(locale.LC_ALL, ('ru_RU', 'UTF8'))
    execute_from_command_line(sys.argv)
