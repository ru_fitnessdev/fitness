# -*- coding: utf-8 -*-
from django.conf import settings
from django.conf.urls import include, url
from django.contrib.auth.views import logout_then_login, login
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.static import serve
from .views import main_page


admin.autodiscover()

urlpatterns = (
    url(r'^api/', include('backend.api.urls', namespace='api')),
    url(r'^login/$', login, name='auth_login'),
    url(r'^logout/$', logout_then_login, name='auth_logout'),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^admin/', include(admin.site.urls), name='admin'),
)

if settings.DEBUG:
    urlpatterns += (url(r'^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT}),)

if getattr(settings, 'ENV', 'production') == 'dev':
    urlpatterns += tuple(staticfiles_urlpatterns())

urlpatterns += (
    url(r'', main_page, name='home'),
)
