# -*- coding: utf-8 -*-
from django.template.response import TemplateResponse
from django.views.decorators.cache import never_cache


@never_cache
def main_page(request):
    """
    Страница нового клиента
    Загружается первой
    :param request: HttpRequest
    """
    return TemplateResponse(request, 'client/index.html', using='jinja2')
