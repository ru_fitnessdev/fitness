# -*- coding: utf-8 -*-
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'fitness'
    }
}
SECRET_KEY = '41f#$%9a!rrh35!e8=6ge_j562&w1wma*ar-(!&p_!)@bchnqq'
DEBUG = True
ADMINS = (
     ('Greg Filippoff', 'mr.filippoff@gmail.com'),
)
MANAGERS = ADMINS

if DEBUG:
    EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
