# -*- coding: utf-8 -*-
# from django.contrib.gis.db.models import PointField
from django.db import models
from django.utils.translation import ugettext_lazy as _

from snippets.models import BaseDictionaryModel


class Country(BaseDictionaryModel):
    slug = models.SlugField(_('Алиас'), unique=True)
    # coords = PointField(_('Координаты'), dim=2)

    class Meta:
        verbose_name = _('Страна')
        verbose_name_plural = _('Страны')


class City(BaseDictionaryModel):
    country = models.ForeignKey(
        Country, verbose_name=_('Страна'), related_name='cities',
        on_delete=models.CASCADE
    )
    slug = models.SlugField(_('Алиас'), unique=True)
    # coords = PointField(_('Координаты'), dim=2)

    class Meta:
        verbose_name = _('Город')
        verbose_name_plural = _('Города')
