# coding: utf-8
from django.contrib import admin
from django.db.models import Count
from django.utils.translation import ugettext_lazy as _
from backend.geo import models
from snippets.admin import SuperUserDeletableAdminMixin, \
    VirtualDeleteAdminMixin, BaseModelAdmin


@admin.register(models.Country)
class CountryAdmin(VirtualDeleteAdminMixin, SuperUserDeletableAdminMixin,
                   BaseModelAdmin):
    list_display = (
        'title', 'slug', 'cities_count', 'status', 'ordering', 'created'
    )
    search_fields = ('=id', 'title', 'slug', 'cities__title')
    list_editable = ('status', 'ordering')
    list_filter = VirtualDeleteAdminMixin.list_filter + ('status',)
    prepopulated_fields = {'slug': ('title',)}
    readonly_fields = VirtualDeleteAdminMixin.readonly_fields + ('created',)
    ordering = ('ordering', 'title')

    def get_queryset(self, request):
        qs = super(CountryAdmin, self).get_queryset(request)
        qs = qs.distinct().annotate(cities_cnt=Count('cities'))
        return qs

    def cities_count(self, obj):
        return getattr(obj, 'cities_cnt', 0)
    cities_count.short_description = _('Кол-во городов')
    cities_count.admin_order_field = 'cities_cnt'


@admin.register(models.City)
class CityAdmin(VirtualDeleteAdminMixin, SuperUserDeletableAdminMixin, 
        BaseModelAdmin):
    """Города"""
    list_display = (
        'title', 'country', 'slug', 'status', 'ordering', 'created'
    )
    search_fields = ('=id', 'title', 'slug', 'country__title')
    list_editable = ('status', 'ordering')
    list_filter = VirtualDeleteAdminMixin.list_filter + ('status', 'country')
    prepopulated_fields = {'slug': ('title',)}
    readonly_fields = VirtualDeleteAdminMixin.readonly_fields + ('created',)
    ordering = ('ordering', 'title')
