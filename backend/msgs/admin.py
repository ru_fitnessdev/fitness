# -*- coding: utf-8 -*-
from suit.admin import SortableModelAdmin
from suit_ckeditor.widgets import CKEditorWidget
from suit.admin import SortableTabularInline
from django.contrib import admin
from django.db import models
from django.contrib.admin import TabularInline
from django.contrib.contenttypes.admin import GenericTabularInline
from snippets.admin import BaseModelAdmin
from backend.photos.models import BasicPhoto
from backend.courses.models import Course, CourseTask, CourseTaskUser
from .models import CourseTaskMessage



class InlineImage(GenericTabularInline):
    model = BasicPhoto
    fields = ('title', 'photo', 'created')
    readonly_fields = ('created',)


@admin.register(CourseTaskMessage)
class CourseTaskMessage(BaseModelAdmin):
    list_display = ['text', 'task', 'task_username', 'created']
    list_filter = ('task__user', 'user', 'created')
    fields = ('user', 'text', 'task', 'created')
    readonly_fields = ('created', 'user')
    inlines = [InlineImage]
