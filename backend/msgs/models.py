# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.contenttypes.fields import GenericRelation
from django.utils.translation import ugettext_lazy as _
from backend.courses.models import CourseTaskUser
from backend.photos.models import BasicPhoto
from backend.users.models import User
from snippets.models.abstract import BaseTextModel



class CourseTaskMessage(BaseTextModel):
    user = models.ForeignKey(User, verbose_name=_('Автор сообщения'),
                             related_name="user_nessages")
    task = models.ForeignKey(CourseTaskUser, verbose_name=_('Задание'),
                             related_name="task_messages")
    photos = GenericRelation(BasicPhoto)

    class Meta:
        verbose_name = _('Сообщение к заданию')
        verbose_name_plural = _('Сообщения')

    def admin_change_url(self):
        _meta = self._meta
        return "/admin/{0}/{1}/{2}/change/".format(
            _meta.app_label,
            _meta.model_name,
            self.pk
        )

    def task_username(self):
        return self.task.user.username

    def task_title(self):
        return self.task.task.title

    task_username.short_description = 'Пользователь задания'
    task_title.short_description = 'Пользователи задания'

    def __str__(self):
        return "#{0}: от {1} к {2}'s task {3}".format(
            self.pk, self.user.username, self.task.user.username, self.task.pk
        )
