# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import
import uuid
import datetime
import logging
import time

from rest_framework.views import APIView
from rest_framework.generics import GenericAPIView
from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework.permissions import IsAuthenticated, AllowAny

from rest_framework_jwt.settings import api_settings
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from django.apps import apps
from django.conf import settings
from django.urls import reverse
from django.shortcuts import get_object_or_404
from snippets.apps import current_site
from snippets.response import response

from backend.users.tasks import send_email
from backend.users.api import serializers

logger = logging.getLogger('raven')

USER_MODEL = apps.get_model(settings.AUTH_USER_MODEL)


class UserModelViewSet(ReadOnlyModelViewSet):
    model = USER_MODEL
    serializer_class = serializers.UserSerializer

    def get_queryset(self):
        return self.model.objects.filter(id=self.request.user.id)


class UserRegister(GenericAPIView):
    """
    Api View  that registers User in system and sends mail for confirm email
    params::ass
    """
    authentication_classes = tuple()
    serializer_class = serializers.AccountSerializer
    permission_classes = (AllowAny,)

    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            email = user.email
            url = '{0}{1}'.format(
                current_site(),
                reverse('api:email_confirm',
                        kwargs={
                            'token': user.token,
                            'uuid': user.alt_id
                        })
            )
            data = {
                'user': user.first_name,
                'url': url,
                'email': email
            }
            result = send_email.delay('confirm_email', [email],
                          'Confirm Email', data)
            return response('Регистрация завершена', 201, serializer.data)
        return response('Ошибка при регистрации', 400, serializer.errors)


class UserProfile(GenericAPIView):
    """
    Api View that updates User profile
    """
    authentication_classes = (JSONWebTokenAuthentication,)
    serializer_class = serializers.ProfileSerializer
    permission_classes = (IsAuthenticated,)
    model = USER_MODEL

    def get_uuid(self, request):
        return request.data.get('uuid', None)

    def get_object(self, request):
        try:
            user = self.model.active_objects.get(
                alt_id=uuid.UUID(self.get_uuid(request) )
            )
            return user
        except Exception:
            return None

    def post(self, request, *args, **kwargs):
        """
        Api View that returns User profile
        """
        user = self.get_object(request)
        if user:
            serializer = self.serializer_class(user)
            return response(None, 200, serializer.data)
        return response('Пользователь не найден', 404)

    def put(self, request, *args, **kwargs):
        """
        Api View that updates User profile (save into  DB)
        """
        user = self.get_object(request)
        if user:
            serializer = self.serializer_class(user, data=request.data)

            if serializer.is_valid():
                serializer.save()
                return response('Изменения сохранены', 200, serializer.data)

            return response('Ошибка при сохранении', 400, serializer.errors)
        return response('Пользователь не найден', 400)


class UserGetJWTToken(GenericAPIView):
    """
    API View  that generates JWT based on user token,
    returns JWT and User payload
    """
    model = USER_MODEL
    permission_classes = (AllowAny,)

    def get(self, request, *args, **kwargs):
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

        token = kwargs.get('token', None)
        user = get_object_or_404(self.model, token=token)
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)
        data = {
            'token': token,
            'user': payload
        }
        return response(None, 200, data)


class ChangePassword(GenericAPIView):
    """
    API View that changes password based on UUID
    """
    authentication_classes = (JSONWebTokenAuthentication,)
    model = USER_MODEL
    permission_classes = (AllowAny,)
    serializer_class = serializers.ChangePasswordSerializer

    def post(self, request, format=None):
        user = get_object_or_404(
            self.model,
            alt_id=uuid.UUID(request.POST.get('uuid'))
        )
        serializer = self.serializer_class(user, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return response('Пароль успешно сохранен', 200)
        return response('Не удалось изменить пароль', 400, serializer.errors)


class RecoverPassword(GenericAPIView):
    """
    API View that sends to user email with recovery-password-link,
    based on email
    """
    permission_classes = (AllowAny,)
    model = USER_MODEL
    serializer_class = serializers.PasswordRecoverySerializer

    def post(self, request, *args, **kwargs):
        email = request.POST.get('email', None)
        if email:
            user = get_object_or_404(self.model, email=email)
            user.token = None  # сгенeрируем новый
            user.save()
            url = '{0}{1}'.format(
                current_site(), reverse('api:new_pass',
                                        kwargs={'token': user.token})
            )
            data = {
                'user': user.get_full_name(),
                'url': url,
                'email': email
            }
            result = send_email.delay('restore_password', [email],
                                      'Password recovery', data)

            while not result.ready():
                time.sleep(1)

            if result.state == 'SUCCESS':
                return response('Письмо для восстановления пароля отправлено'
                                ' на ваш email', 200)
            else:
                return response('Произошла ошибка. '
                                'Попробуйте ещё раз', 400)


class ConfirmEmail(APIView):

    permission_classes = (AllowAny,)
    model = USER_MODEL

    def get(self, request, *args, **kwargs):
        uuid = kwargs.get('uuid', None)
        token = kwargs.get('token', None)

        if uuid and token:
            try:
                user = self.model.active_objects.get(token=token, alt_id=uuid)
                user.email_verified_date = datetime.datetime.now()
                user.token = None  # сгенeрируем новый
                user.save()
                return response('Ваш Email успешно подтвержден', 200)
            except:
                pass
        return response('Не удалось подтвердить ваш Email', 400)
