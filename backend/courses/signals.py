# -*- coding: utf-8 -*-
from django.db.models.signals import post_save
from backend.users.models import User
from backend.users.enum import UserRoleEnum
from snippets.models.enumerates import StatusEnum
from .models import CourseTask, CourseTaskUser
from .enum import CourseTaskEnum, UserTaskEnum


def user_task_create(sender, instance, **kwargs):
    if instance.is_active and \
       instance.type == CourseTaskEnum.TASK and \
       instance.status == StatusEnum.PUBLIC:
        for user in User.objects.filter(role=UserRoleEnum.USER):
            if instance not in user.tasks.all():
                ctu = CourseTaskUser(
                    user=user,
                    task=instance,
                    task_status=UserTaskEnum.NOT_STARTED
                )
                ctu.save()

post_save.connect(user_task_create, sender=CourseTask)
