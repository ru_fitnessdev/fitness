# -*- coding: utf-8 -*-
from django import forms
from django.contrib.contenttypes.admin import BaseGenericInlineFormSet
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _


class HandbookAdminForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(HandbookAdminForm, self).__init__(*args, **kwargs)
        self.fields['content_types'].queryset = Handbook.get_content_types()

    class Meta:
        model = Handbook
        fields = '__all__'
        widgets = {
            'content_types': autocomplete_light.MultipleChoiceWidget(
                'ContentTypeHandbookAutocomplete'
            ),
            'projects': autocomplete_light.MultipleChoiceWidget(
                'ProjectAutocomplete'
            )
        }


class HBValueFormSet(BaseGenericInlineFormSet):

    def clean(self):
        cleaned_data = super(HBValueFormSet, self).clean()
        hb_ids = set()

        for form in self.forms:
            value = form.cleaned_data.get('value', None)
            handbook = form.cleaned_data.get('handbook', None)
            delete = form.cleaned_data.get('DELETE')
            if value and handbook and not delete:
                if handbook.id in hb_ids:
                    raise ValidationError(
                        'Вы можете добавить только одно значение %s' %
                        handbook
                    )
                else:
                    hb_ids.update({handbook.id})
        return cleaned_data


class HBValueForm(forms.ModelForm):
    model = HBValue

    def clean(self):
        cleaned_data = super(HBValueForm, self).clean()

        value = cleaned_data.get('value', None)
        handbook = cleaned_data.get('handbook', None)

        if value and handbook:
            if handbook.is_only_values_from_items:
                if not handbook.items.filter(value=value).exists():
                    raise ValidationError(
                        'Не верное значение %s. (Оно отсутствует в списке '
                        'возможных значений справочника)' % value
                    )
            else:
                HandbookItem.objects.get_or_create(
                    handbook=handbook,
                    value=HandbookItem.prepare_value(value)
                )
        return cleaned_data

    class Meta:
        fields = '__all__'
        widgets = {
            'handbook': autocomplete_light.ChoiceWidget('HandbookAutocomplete'),
            'value': autocomplete_light.ChoiceWidget('HandbookItemAutocomplete')
        }


class HandbookItemFormSet(forms.models.BaseInlineFormSet):

    def _construct_form(self, i, **kwargs):
        kwargs['parent_object'] = self.instance
        return super(HandbookItemFormSet, self)._construct_form(i, **kwargs)


class HandbookItemForm(forms.ModelForm):
    model = HandbookItem

    def __init__(self, *args, **kwargs):
        self.parent_object = kwargs.pop('parent_object', None)
        super(HandbookItemForm, self).__init__(*args, **kwargs)
        if self.parent_object:
            if self.parent_object.id:
                self.fields['parent'].queryset = HandbookItem.objects\
                    .filter(handbook=self.parent_object)\
                    .order_by('tree_id', 'lft')

                if self.instance.id:
                    self.fields['parent'].queryset = self.fields['parent']\
                        .queryset.exclude(id=self.instance.id)
            else:
                self.fields['parent'].queryset = HandbookItem.objects.none()
        else:
            self.fields['parent'].queryset = HandbookItem.objects.none()


class HandbookItemUploadForm(forms.Form):
    IMPORT_FILE_TYPES = ['.xls']
    handbook = forms.ModelChoiceField(Handbook.objects.all(), label='Классификатор')
    xls_file = forms.FileField(label='Файл')

    def clean(self):
        cleaned_data = super(HandbookItemUploadForm, self).clean()

        if 'xls_file' not in cleaned_data:
            raise forms.ValidationError('Файл XLS обязателен')

        if 'handbook' not in cleaned_data:
            raise forms.ValidationError('Необходимо выбрать классификатор')

        input_excel = cleaned_data.get('xls_file')
        extension = os.path.splitext(input_excel.name)[1]
        if not (extension in self.IMPORT_FILE_TYPES):
            raise forms.ValidationError(
                '%s не является файлом Excel. '
                'Убедитесь в том, что ваш файл в формате XLS '
                '(Excel 2007 НЕ поддерживается.)' % input_excel.name
            )

        file_data = six.StringIO()
        for chunk in input_excel.chunks():
            file_data.write(chunk)
        cleaned_data['xls_file'] = file_data.getvalue()
        file_data.close()

        try:
            cleaned_data['workbook'] = xlrd.open_workbook(file_contents=cleaned_data['xls_file'])
        except xlrd.XLRDError, e:
            raise forms.ValidationError(_('Unable to open XLS file: %s' % e))

        return cleaned_data
