# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from snippets.models.abstract import BasePostModel, StatusOrderingModel,\
    BaseIsActiveModel
from backend.users.models import User
from .enum import UserTaskEnum, CourseTaskEnum


class Course(BasePostModel, BaseIsActiveModel, StatusOrderingModel):
    class Meta:
        verbose_name = _('Курс')
        verbose_name_plural = _('Курсы')
        ordering = ['ordering', 'id']

    def __str__(self):
        return self.title


class CourseTask(BasePostModel, BaseIsActiveModel, StatusOrderingModel):
    course = models.ForeignKey(Course, verbose_name=_('Курс'))
    type = models.CharField(
        _('Тип'),
        choices=CourseTaskEnum.get_choices(),
        default=CourseTaskEnum.ARTICLE, max_length=12
    )

    class Meta:
        verbose_name = _('Статья(задание)')
        verbose_name_plural = _('Статьи')
        ordering = ['ordering', 'course']

    def __str__(self):
        return "{0} ({1})".format(self.title, self.course)

    def admin_change_url(self):
        _meta = self._meta
        return "/admin/{0}/{1}/{2}/change/".format(
            _meta.app_label,
            _meta.model_name,
            self.pk
        )


class CourseTaskUser(models.Model):
    task = models.ForeignKey(CourseTask, verbose_name=_('Задание'))
    user = models.ForeignKey(User, verbose_name=_('Пользоветль-клиент'),
                             related_name="mytasks")
    task_status = models.CharField(
        _('Статус задания'),
        choices=UserTaskEnum.get_choices(),
        default=UserTaskEnum.NOT_STARTED, max_length=32
    )

    class Meta:
        verbose_name = _('Задание')
        verbose_name_plural = _('Задания')
        unique_together = (("user", "task"))

    def __str__(self):
        return "{0}-{1}:{2}".format(
            self.task.course,
            self.task.title,
            self.get_task_status_display()
        )

    def admin_change_url(self):
        _meta = self._meta
        return "/admin/{0}/{1}/{2}/change/".format(
            _meta.app_label,
            _meta.model_name,
            self.pk
        )

"""
class CourseTaskUserReport(BaseTextModel):
    task_user = models.ForeignKey(CourseTaskUser,
                                  related_name='user_task_reports',
                                  verbose_name=_('Задание'))

    class Meta:
        verbose_name = _('Ответ на задание')
        verbose_name_plural = _('Ответы на задание')

    def task_username(self):
        return self.task_user.user

    def task_title(self):
        return self.task_user.task

    def __str__(self):
        return "{0}: {1}".format(self.task_user.user.get_full_name(),
                                 self.task_user.task.title)

"""
