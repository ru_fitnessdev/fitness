# -*- coding: utf-8 -*-
from django.apps import AppConfig as BaseAppConfig
from django.utils.translation import ugettext_lazy as _


class AppConfig(BaseAppConfig):
    name = 'backend.courses'
    verbose_name = _('Курсы занятий')

    def ready(self):
        from backend.courses import signals  # flake8: NOQA