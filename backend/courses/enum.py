# -*- coding: utf-8 -*-
from collections import OrderedDict
from django.utils.translation import ugettext_lazy as _
from snippets.models.enumerates import BaseEnumerate


class UserTaskEnum(BaseEnumerate):
    NOT_STARTED = 'NOT_STARTED'
    PROCESS = 'PROCESS'
    FAIL = 'FAIL'
    DONE = 'DONE'
    css = {
        NOT_STARTED: 'warning',
        PROCESS: 'info',
        FAIL: 'error',
        DONE: 'success'
    }

    values = OrderedDict((
        (NOT_STARTED, _('Не стартовал')),
        (PROCESS, _('В процессе')),
        (FAIL, _('Провалено')),
        (DONE, _('Выполнено')),
    ))


class CourseTaskEnum(BaseEnumerate):
    ARTICLE = 'a'
    TASK = 't'
    values = OrderedDict((
        (ARTICLE, _('Статья')),
        (TASK, _('Задание')),
    ))
