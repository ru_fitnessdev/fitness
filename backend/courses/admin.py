# -*- coding: utf-8 -*-
from suit.admin import SortableModelAdmin
from suit_ckeditor.widgets import CKEditorWidget
from django.contrib import admin
from django.db import models
from django.contrib.admin import TabularInline
from django.contrib.contenttypes.admin import GenericTabularInline

from backend.msgs.models import CourseTaskMessage
from backend.courses.enum import UserTaskEnum
from backend.courses.models import Course, CourseTask, CourseTaskUser
from snippets.admin import BaseModelAdmin
from backend.photos.models import BasicPhoto


class CourseTaskMessageInline(TabularInline):
    model = CourseTaskMessage
    extra = 0
    fields = ('text', 'user', 'created', 'count_photos_link')
    readonly_fields = ('user', 'created', 'count_photos_link',)

    def count_photos_link(self, obj):
        if obj.id:
            total_photos = obj.photos.count()
            if total_photos:
                return '<a href="%s" target="_blank">%d</a>'\
                 % (obj.admin_change_url(), total_photos)
        return '---'

    count_photos_link.allow_tags = True
    count_photos_link.short_description = 'Photos?'


class CourseTaskInline(TabularInline):
    model = CourseTask
    extra = 0
    fields = ('title', 'type', 'status', 'is_active', 'changeform_link')
    readonly_fields = ('changeform_link',)

    def changeform_link(self, obj):
        if obj.id:
            return '<a href="%s" target="_blank">Редактировать</a>'\
             % obj.admin_change_url()
        return 'Объект еще не создан. Нажмите кнопку '\
               '"Сохранить и продолжить редактирование"'

    changeform_link.allow_tags = True
    changeform_link.short_description = 'Текст'

class InlineImage(GenericTabularInline):
    model = BasicPhoto
    readonly_fields = ('type_update',)


@admin.register(Course)
class Course(SortableModelAdmin):
    list_display = ['title', 'status', 'ordering', 'is_active']
    list_editable = ['ordering']
    list_filter = ('status', 'is_active')
    sortable = 'ordering'
    fields = ('title', 'status', 'text', 'is_active')
    inlines = [CourseTaskInline]
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget()}
    }


@admin.register(CourseTask)
class CourseTask(SortableModelAdmin):
    list_display = ['title', 'type', 'course', 'status', 'ordering',
                    'is_active']
    list_editable = ['ordering']
    list_filter = ('status', 'is_active', 'type', 'course')
    fields = ('title', 'course', 'type', 'status', 'text', 'is_active')
    search_fields = ['title', 'course__title', 'text']
    sortable = 'ordering'
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget()}
    }


@admin.register(CourseTaskUser)
class CourseTaskUser(BaseModelAdmin):
    inlines = [CourseTaskMessageInline]
    list_filter = ('task', 'user', 'task_status')
    list_display = ['task', 'user', 'task_status']
    search_fields = ['task__user__username', 'task__title', 'task__text']
    # readonly_fields = ('task', 'user')

    def has_add_permission(self, request):
        return False

    def suit_row_attributes(self, obj, request):
        status = str(obj.task_status)
        return {
            'class': UserTaskEnum.get_css_by_name(status),
            'data': obj.task.title
        }

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for instance in instances:
            instance.user = request.user
            instance.save()
        formset.save_m2m()
