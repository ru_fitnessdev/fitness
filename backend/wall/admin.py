# -*- coding: utf-8 -*-
from django.contrib import admin
from mptt.admin import MPTTModelAdmin
# from django.contrib.admin import StackedInline, TabularInline
# from django.contrib.auth.admin import UserAdmin, GroupAdmin
# from django.contrib.auth.models import Group
# from django.utils.translation import ugettext_lazy as _
from backend.wall.models import Wall, Likes
from snippets.admin import BaseModelAdmin


@admin.register(Wall)
class WallAdmin(BaseModelAdmin, MPTTModelAdmin):
    list_display = ('user', 'text', 'created', 'likes_count')
    search_fields = (
        '=id', 'user__first_name', 'user__last_name', 'user__email'
    )
    list_filter = ('user', 'user__is_active')


#@admin.register(Likes)
class LikeAdmin(BaseModelAdmin):
    pass
