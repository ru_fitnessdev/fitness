# coding: utf-8
from django.db.models import Manager


class CommentManager(Manager):
    def get_queryset(self):
        return super(CommentManager, self).get_queryset().filter(level__gte=2)
