# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework import serializers
from backend.courses.models import Course, CourseTask, CourseTaskUser


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'phone', 'username',
                  'alt_id', 'email', 'is_active']


class AccountSerializer(serializers.ModelSerializer):
    password = serializers.CharField(
        write_only=True,
        required=True,
        validators=[CustomPasswortRestValidator()]
    )
    confirm_password = serializers.CharField(write_only=True, required=True)
    last_name = serializers.CharField(write_only=True, required=True)
    first_name = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = User
        fields = (
            'username', 'email', 'first_name', 'last_name',
            'password', 'confirm_password'
        )

    def create(self, validated_data):
        return User.objects.create_user(**validated_data)

    def validate(self, data):
        if data['password']:
            if data['password'] != data['confirm_password']:
                raise serializers.ValidationError(
                    "Пароли не совпадают"
                )
        data.pop('confirm_password')
        return data


class RecoverPasswordSerializer(serializers.ModelSerializer):
    email = serializers.EmailField()


class ChangePasswordSerializer(serializers.ModelSerializer):
    password = serializers.CharField(
        write_only=True,
        required=False,
        validators=[CustomPasswortRestValidator()]
    )
    confirm_password = serializers.CharField(write_only=True, required=False)
    uuid = serializers.CharField(write_only=True, required=False)

    def update(self, instance, validated_data):
        password = validated_data.get('password', None)
        confirm_password = validated_data.get('confirm_password', None)

        if password and password == confirm_password:
            instance.set_password(password)

        instance.token = None
        instance.save()
        return instance

    class Meta:
        model = User
        fields = ('password', 'confirm_password', 'uuid')


class PasswordRecoverySerializer(serializers.ModelSerializer):
    email = serializers.EmailField(allow_blank=False)

    class Meta:
        model = User
        fields = ('email',)


class ProfileSerializer(serializers.ModelSerializer):
    password = serializers.CharField(
        write_only=True,
        required=False,
        validators=[CustomPasswortRestValidator()]
    )
    confirm_password = serializers.CharField(write_only=True, required=False)

    class Meta:
        model = User
        fields = (
            'email', 'first_name', 'last_name', 'gender',
            'city', 'phone', 'user_description', 'social_instagram',
            'social_vkontakte', 'photo', 'password', 'confirm_password'
        )

    def update(self, instance, validated_data):
        instance.email = validated_data.get('email', instance.email)
        instance.first_name = validated_data.get('first_name',
                                                 instance.first_name)
        instance.last_name = validated_data.get('last_name',
                                                instance.last_name)
        instance.city = validated_data.get('city', instance.city)
        instance.phone = validated_data.get('phone', instance.phone)
        instance.user_description = validated_data.get(
            'user_description', instance.user_description)
        instance.social_instagram = validated_data.get(
            'social_instagram', instance.social_instagram)
        instance.social_vkontakte = validated_data.get(
            'social_vkontakte', instance.social_vkontakte)
        instance.photo = validated_data.get(
            'photo', instance.photo)

        password = validated_data.get('password', None)
        confirm_password = validated_data.get('confirm_password', None)

        if password and password == confirm_password:
            instance.set_password(password)

        instance.save()
        return instance
