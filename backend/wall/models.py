# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
from django.contrib.contenttypes.fields import GenericRelation
from django.utils.translation import ugettext_lazy as _
from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel
from snippets.models.abstract import BaseTextModel, ContentTypeModel
from .managers import CommentManager


class Wall(MPTTModel, BaseTextModel):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name='wall',
        verbose_name=_('Пользователь'), on_delete=models.CASCADE
    )
    parent = TreeForeignKey(
        'self', verbose_name='Родительская запись',
        null=True, blank=True, related_name='children', db_index=True
    )
    likes = GenericRelation('Likes')
    comments = CommentManager()

    def __str__(self):
        created = self.created
        return '{0}[{1}]: {2}'.format(
            self.user.username, created.strftime("%d/%m/%Y %H:%m"), self.text
        )

    def likes_count(self):
        return self.likes.count()

    likes_count.short_description = 'Кол-во лайков'

    class Meta:
        verbose_name = _('Записи')
        verbose_name_plural = _('Записи')


class Likes(ContentTypeModel):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name='likes',
        verbose_name=_('Пользователь'), on_delete=models.CASCADE
    )

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name = _('Лайк')
        verbose_name_plural = _('Лайки')
        unique_together = (("user", "object_id", "content_type"))
