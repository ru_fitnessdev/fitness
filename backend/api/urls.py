# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf import urls
from backend.api.swagger import SwaggerSchemaView


urlpatterns = (
    urls.url(
        r'^auth/',
        urls.include('backend.users.api.urls'),
        name='users'
    ),
    urls.url(
        r'^courses/',
        urls.include('backend.courses.api.urls'),
        name='courses'
    ),
    urls.url(
        r'^wall/',
        urls.include('backend.wall.api.urls'),
        name='courses'
    ),
    urls.url(
        r'^',
        SwaggerSchemaView.as_view(),
        {'title': 'API Section'},
        name='swagger'
    ),
)

"""
urls.url(
    r'^auth/$',
    views.AuthAPIView.as_view(), name='auth'
),
"""
