# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from rest_framework_swagger.renderers import OpenAPIRenderer, SwaggerUIRenderer
from rest_framework.schemas import SchemaGenerator
from rest_framework.response import Response


class SwaggerSchemaView(APIView):
    permission_classes = [AllowAny]
    renderer_classes = [
       OpenAPIRenderer,
       SwaggerUIRenderer
    ]

    def get(self, request, *args, **kwargs):
        title = kwargs.get('title', "API")
        generator = SchemaGenerator(title=title)
        schema = generator.get_schema(request=request)
        return Response(schema)
