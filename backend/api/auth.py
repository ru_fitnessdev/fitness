# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from calendar import timegm
from datetime import datetime
from backend.users.api.serializers import UserSerializer


# Обогащаем данные возвращаемые jwt app:
def jwt_response_payload_handler(token, user=None, request=None):
    return {
        'token': token,
        'user': UserSerializer(user, context={'request': request}).data
    }


# Аутентификация по email:
def jwt_get_username_from_payload_handler(payload):
    return payload.get('email')


def jwt_response_payload(user):
    data = UserSerializer(user).data
    data.update({
        'iss': 'fitness_motherfucker',
        'orig_iat': timegm(datetime.utcnow().utctimetuple())})
    return data
