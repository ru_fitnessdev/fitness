from __future__ import absolute_import, unicode_literals
from main.celery import app
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from raven.contrib.django.raven_compat.models import client as sentrycli


class BaseTask(app.Task):
    abstract = True

    def on_retry(self, exc, task_id, args, kwargs, einfo):
        sentrycli.captureException()
        super(BaseTask, self).on_retry(exc, task_id, args, kwargs, einfo)

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        sentrycli.captureException()
        super(BaseTask, self).on_failure(exc, task_id, args, kwargs, einfo)


@app.task(bind=True, max_retries=3,  soft_time_limit=5, base=BaseTask)
def send_email(self, template, emails, subject, params=None,
               extra_headers=None, from_email=None):
    from_email = settings.DEFAULT_FROM_EMAIL
    message_html = render_to_string(
        'emails/%s/%s.html' % (template, template), params, using='jinja2'
    )
    message_txt = render_to_string(
        'emails/%s/%s.txt' % (template, template), params, using='jinja2'
    )
    msg = EmailMultiAlternatives(subject, message_txt, from_email, emails,
                                 headers=extra_headers)
    msg.attach_alternative(message_html, 'text/html')

    try:
        msg.send()
    except Exception as exc:
        raise self.retry(countdown=5, exc=exc)
