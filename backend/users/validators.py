# -*- coding: utf-8 -*-
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext as _
from rest_framework import serializers


class CustomPasswortRestValidator(object):
    def __init__(self):
        self.min_length = 8

    def __call__(self, value):
        if len(value) < self.min_length:
            raise serializers.ValidationError(
                _('Пароль должен состоять как минимум из %(min_length)d'
                  'символов') % {"min_length": self.min_length}
            )

        if not any(char.isdigit() for char in value):
            raise serializers.ValidationError(
                _('Пароль должен содержать хотя бы 1 цифру')
            )

        if not any(char.isalpha() for char in value):
            raise serializers.ValidationError(
                _('Пароль должен содержать хотя бы 1 буковку')
            )
