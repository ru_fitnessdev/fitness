# -*- coding: utf-8 -*-
from random import randint
from django.db.models.signals import pre_save
from snippets.utils.passwords import generate_random_string
from .models import User


def set_alt_id(sender, instance, **kwargs):
    if not instance.token:
        max_len = instance._meta.get_field('token').max_length
        length = randint(128, max_len)
        instance.token = generate_random_string(length=length)

pre_save.connect(set_alt_id, sender=User)
