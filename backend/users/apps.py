# -*- coding: utf-8 -*-
from django.apps import AppConfig as BaseAppConfig
from django.utils.translation import ugettext_lazy as _


class AppConfig(BaseAppConfig):
    name = 'backend.users'
    verbose_name = _('Профили пользователей')

    def ready(self):
        from backend.users import signals
