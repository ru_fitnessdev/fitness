# coding: utf-8
from collections import OrderedDict

from django.utils.translation import ugettext_lazy as _

from snippets.models.enumerates import BaseEnumerate


class UserRoleEnum(BaseEnumerate):
    """Тип роли пользователя"""
    USER = 'user'
    MANAGER = 'manager'
    SUPERMANAGER = 'supermanager'

    values = OrderedDict((
        (USER, _('Пользователь')),
        (MANAGER, _('Менеджер')),
        (SUPERMANAGER, _('СуперМенеджер'))
    ))

    @classmethod
    def is_project_staff(cls, value):
        return value in (cls.MANAGER, cls.SUPERMANAGER)


class UserGenderEnum(BaseEnumerate):
    """
    Перечисление статусов объектов
    """
    MALE = 'm'
    FEMALE = 'f'
    

    values = OrderedDict((
        (MALE, _('Мужской')),
        (FEMALE, _('Женский')),
    ))
