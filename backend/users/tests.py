import json
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from backend.users.models import User
import unittest
from django.test import Client, TestCase
from django.db import transaction

PASS = 'password11'
USERNAME = 'test@test.ru'


class AccountTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.client = Client()
        cls.user = None

    def test_create_account(self):
        register_data = {
            'email': USERNAME,
            'username': USERNAME,
            'password': PASS,
            'confirm_password': PASS,
            'first_name': 'greg',
            'last_name': 'gregoff',
            'nickname': 'greguar',
            'phone': '891010101',
            'is_active': True
        }
        url = reverse('api:register_user')
        response = self.client.post(url, register_data)
        self.user = User.objects.get(email=USERNAME)
        self.assertEqual(response.status_code, 201)
        print(self.user) # <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


    def test_get_account(self):
        print(self.user) # <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        