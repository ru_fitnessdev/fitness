# -*- coding: utf-8 -*-
import uuid
import os
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from easy_thumbnails.fields import ThumbnailerImageField
# from easy_thumbnails.files import get_thumbnailer
from snippets.models.abstract import LastModModel
from snippets.models.managers import IsActiveManager
from snippets.utils.passwords import generate_random_string
from .enum import UserRoleEnum, UserGenderEnum
from .managers import UserManager
from backend.geo.models import City


def init_images_path(instance, filename, path):
    parts = filename.split('.')
    ext = parts[-1] if len(parts) else 'jpg'
    return "%s/%s.%s" % (path, instance.alt_id, ext)


def get_upload_photo(instance, filename):
    return init_images_path(instance, filename, 'images/photos')


def get_upload_avatar(instance, filename):
    return init_images_path(instance, filename, 'images/avatars')

IMAGES_FOLDER = 'images'

AVATARS_ROOT = os.path.join(
                settings.MEDIA_ROOT, IMAGES_FOLDER, 'user_avatars'
            )

PHOTOS_ROOT = os.path.join(
                settings.MEDIA_ROOT, IMAGES_FOLDER, 'user_photos'
            )


class User(AbstractUser, LastModModel):
    REQUIRED_FIELDS = ['username']
    USERNAME_FIELD = 'email'
    email = models.EmailField(_('E-mail'), unique=True)
    gender = models.CharField(
        _('Пол'), choices=UserGenderEnum.get_choices(),
        default=UserGenderEnum.MALE, max_length=30
    )
    user_description = models.TextField(
        _('Обо мне'), null=True, blank=True
    )
    role = models.CharField(
        _('Роль в системе'), choices=UserRoleEnum.get_choices(),
        default=UserRoleEnum.USER, max_length=30
    )
    city = models.ForeignKey(
        City, verbose_name=_('Город'), related_name='users', blank=True,
        null=True, on_delete=models.SET_NULL
    )
    phone = models.CharField(
        verbose_name=_('Телефон'), blank=True, null=True, max_length=16
    )
    born = models.DateField(
        _('Дата рождения'), auto_now_add=False, blank=True, null=True
    )
    email_verified_date = models.DateTimeField(
        _('Дата подтверждения email'), blank=True, null=True
    )
    alt_id = models.UUIDField(
        _('Доп. ID'), default=uuid.uuid4, editable=False
    )
    token = models.CharField(
        _('Tокен'), max_length=149, default=generate_random_string,
        blank=True, null=True
    )
    social_instagram = models.URLField(
        _('Профиль в инстаграм'), null=True, blank=True
    )
    social_vkontakte = models.URLField(
        _('Профиль в вконтакте'), null=True, blank=True
    )
    photo = ThumbnailerImageField(
        _('Фотография'), upload_to=get_upload_photo,
        blank=True, null=True
    )
    avatar = ThumbnailerImageField(
        _('Аватар'), upload_to=get_upload_avatar,
        blank=True, null=True
    )
    # tasks = models.ManyToManyField(
    #    'courses.CourseTask', through='courses.CourseTaskUser',
    #    related_name='course_tasks', verbose_name=_('Задания пользователя')
    # )
    objects = UserManager()
    active_objects = IsActiveManager()

    def __str__(self):
        return self.get_full_name()

    def get_full_name(self):
        parts = filter(
            None, (self.last_name, self.first_name,)
        )
        full_name = ' '.join(parts)
        return full_name or self.username

    get_full_name.short_description = 'Полное имя'

    def posts(self):
        return self.wall.filter(level=0)

    class Meta:
        verbose_name = _('Профиль пользователя')
        verbose_name_plural = _('Профили пользователей')
