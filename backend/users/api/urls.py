# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf import urls
# from rest_framework import routers
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token,\
                                    verify_jwt_token
from backend.users.api import views
from backend.api.swagger import SwaggerSchemaView


# У нас есть jwt токен, а есть просто токен (хеш-строка у User)
# НЕ ПУТАТЬ ИХ!!!

urlpatterns = (
    # Аутентификация (POST: email/password)
    # получение jwt токена для заголовка 'Authorization'
    urls.url(
        r'^api-token-auth/',
        obtain_jwt_token,
        name='get_token'
    ),
    # Обновление jwt токена (POST: token) для заголовка 'Authorization':
    urls.url(
        r'^api-token-refresh/',
        refresh_jwt_token,
        name='update_token'
    ),
    # Проверка jwt токена (POST: token) для заголовка 'Authorization':
    urls.url(
        r'^api-token-verify/',
        verify_jwt_token,
        name='check_token'
    ),
    # Регистрация пользователя
    # (POST: username/email/first_name/last_name/password/confirm_password':
    urls.url(
        r'^register/$',
        views.UserRegister.as_view(),
        name='register_user'
    ),
    # Данные профиля пользователя(POST: uuid),
    # редактирование профиля(PUT: [profile fields]):
    urls.url(
        r'^profile/$',
        views.UserProfile.as_view(),
        name="user_profile"
    ),
    # Ссылка для восстановления пароля
    # Получение jwt токена по User.token
    # jwt токен необходим, чтобы авторизовать пользователя:
    # смена пароля - только при наличии заголовка
    # 'Authorization':'JWT <jwt_token>')
    urls.url(
        r'^password-new/(?P<token>[^/]+)',
        views.UserGetJWTToken.as_view(),
        name='new_pass'
    ),
    # Получение формы с измененным паролем
    # (POST: password/confirm_password/
    urls.url(
        r'^password-change/$',
        views.ChangePassword.as_view(),
        name='change_pass'
    ),
    # Форма с email для восстановления пароля (POST: email)
    # Отправка письма с ссылкой /password-new/<token>
    urls.url(
        r'^password-recover/',
        views.RecoverPassword.as_view(),
        name='make_pass_request'
    ),
    # Подтверждение email (GET: uuid/token):
    urls.url(
        r'^email-confirm/(?P<token>[^/]+)/(?P<uuid>[^/]+)/',
        views.ConfirmEmail.as_view(),
        name='email_confirm'
    ),

)
