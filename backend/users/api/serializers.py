# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from drf_extra_fields.fields import Base64ImageField
from rest_framework import serializers
# from django.core.exceptions import ValidationError
from backend.users.models import User
from backend.users.validators import CustomPasswortRestValidator
# from rest_framework.fields import ImageField
# from easy_thumbnails.templatetags.thumbnail import thumbnail_url
import base64


class HybridImageField(Base64ImageField):

    def to_internal_value(self, data):
        try:
            return Base64ImageField.to_internal_value(self, data)
        except ValidationError:
            return ImageField.to_internal_value(self, data)

    def to_representation(self, file):
        if self.represent_in_base64:
            try:
                with open(file.path, 'rb') as f:
                    return base64.b64encode(f.read()).decode()
            except Exception:
                pass
        if file:
            return super(Base64ImageField, self).to_representation(file)


class UserSerializer(serializers.ModelSerializer):
    photo = HybridImageField(required=False)
    avatar = HybridImageField(required=False)

    class Meta:
        model = User
        fields = (
            'email', 'first_name', 'last_name', 'gender',
            'city', 'phone', 'user_description', 'social_instagram',
            'social_vkontakte', 'photo', 'avatar', 'email_verified_date',
            'username', 'alt_id'
        )


class AccountSerializer(serializers.ModelSerializer):
    password = serializers.CharField(
        write_only=True,
        required=True,
        validators=[CustomPasswortRestValidator()]
    )
    confirm_password = serializers.CharField(write_only=True, required=True)
    last_name = serializers.CharField(write_only=True, required=True)
    first_name = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = User
        fields = (
            'username', 'email', 'first_name', 'last_name',
            'password', 'confirm_password'
        )

    def create(self, validated_data):
        return User.objects.create_user(**validated_data)

    def validate(self, data):
        if data['password']:
            if data['password'] != data['confirm_password']:
                raise serializers.ValidationError(
                    "Пароли не совпадают"
                )
        data.pop('confirm_password')
        return data


class RecoverPasswordSerializer(serializers.ModelSerializer):
    email = serializers.EmailField()


class ChangePasswordSerializer(serializers.ModelSerializer):
    password = serializers.CharField(
        required=True,
        validators=[CustomPasswortRestValidator()]
    )
    confirm_password = serializers.CharField(required=True)
    token = serializers.CharField(required=True)

    def update(self, instance, validated_data):
        password = validated_data.get('password', None)
        confirm_password = validated_data.get('confirm_password', None)

        if password and password == confirm_password:
            instance.set_password(password)

        instance.token = None
        instance.save()
        return instance

    class Meta:
        model = User
        fields = ('password', 'confirm_password', 'token')


class PasswordRecoverySerializer(serializers.ModelSerializer):
    email = serializers.EmailField(allow_blank=False)

    class Meta:
        model = User
        fields = ('email',)


class ProfileSerializer(serializers.ModelSerializer):
    password = serializers.CharField(
        write_only=True,
        required=False,
        validators=[CustomPasswortRestValidator()]
    )
    confirm_password = serializers.CharField(write_only=True, required=False)
    photo = HybridImageField(required=False, allow_null=True,
                             represent_in_base64=True)
    avatar = HybridImageField(required=False, allow_null=True,
                              represent_in_base64=True)

    class Meta:
        model = User
        fields = (
            'email', 'first_name', 'last_name', 'gender',
            'city', 'phone', 'user_description', 'social_instagram',
            'social_vkontakte', 'photo', 'password', 'confirm_password',
            'username', 'avatar'
        )

    def update(self, instance, validated_data):
        instance.username = validated_data.get('username', instance.username)
        instance.first_name = validated_data.get('first_name',
                                                 instance.first_name)
        instance.last_name = validated_data.get('last_name',
                                                instance.last_name)
        instance.city = validated_data.get('city', instance.city)
        instance.gender = validated_data.get('gender', instance.city)
        instance.phone = validated_data.get('phone', instance.phone)
        instance.user_description = validated_data.get(
            'user_description', instance.user_description)

        instance.social_instagram = validated_data.get(
            'social_instagram', instance.social_instagram)

        instance.social_vkontakte = validated_data.get(
            'social_vkontakte', instance.social_vkontakte)

        instance.photo = validated_data.get('photo')
        instance.avatar = validated_data.get('avatar')

        password = validated_data.get('password', None)
        confirm_password = validated_data.get('confirm_password', None)

        if password and password == confirm_password:
            instance.set_password(password)

        instance.save()
        return instance
