# -*- coding: utf-8 -*-
from django.contrib import admin
from django.db import models
# from django.contrib.admin import StackedInline, TabularInline
from suit.admin import SortableModelAdmin
from suit_ckeditor.widgets import CKEditorWidget
from suit.admin import SortableTabularInline, SortableModelAdmin,\
    SortableStackedInline
from django.contrib.auth.admin import UserAdmin
from django.contrib.admin import TabularInline
from django.contrib.admin import SimpleListFilter
from django.utils.translation import ugettext_lazy as _
from easy_thumbnails.widgets import ImageClearableFileInput
from easy_thumbnails.fields import ThumbnailerImageField

from backend.users.models import User
from snippets.admin import activate_action, deactivate_action, BaseModelAdmin
from backend.wall import models as wall_models
from backend.courses import models as courses_models
# from courses.enum import UserTaskEnum
from backend.msgs.models import CourseTaskMessage



class CTasksFilter(SimpleListFilter):
    title = 'Задания'
    parameter_name = 'tasks_ss'

    def lookups(self, request, model_admin):
        return [
            (c.id, c.title)
            for c in courses_models.CourseTask.objects.filter(is_active=True)
        ]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(tasks=self.value())
        else:
            return queryset


class WallInline(TabularInline):
    model = wall_models.Wall
    extra = 0
    fields = ('text', 'created')
    readonly_fields = ('created', 'text')
    suit_classes = 'suit-tab suit-tab-wall'
    verbose_name = _('Лента пользователя')
    ordering = ('created',)

    def get_queryset(self, request):
        qs = super(WallInline, self).get_queryset(request)
        return qs.filter(level=0)

    def has_add_permission(self, request):
        return False


class CourseTaskMessageInline(TabularInline):
    model = CourseTaskMessage
    extra = 0
    suit_classes = 'suit-tab suit-tab-msgs'
    readonly_fields = ('task', 'text', 'created', 'changeform_link')
    fields = ('task', 'text', 'created', 'changeform_link')

    def changeform_link(self, obj):
        if obj.id:
            return '<a href="%s" target="_blank">Перейти</a>'\
             % obj.task.admin_change_url()
        return """
                Объект еще не создан. Нажмите кнопку
               "Сохранить и продолжить редактирование"
               """
    changeform_link.allow_tags = True
    changeform_link.short_description = ''

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "task":
            try:
                kwargs["queryset"] = courses_models.CourseTaskUser.\
                    objects.filter(user=request.resolver_match.args[0])
            except IndexError:
                pass
        return super(
            CourseTaskMessageInline, self).formfield_for_foreignkey(
                db_field, request, **kwargs
            )

    def has_add_permission(self, request):
        return False


class CourseTaskUserInline(TabularInline):
    model = courses_models.CourseTaskUser
    extra = 0
    fields = ('task', 'task_status')
    suit_classes = 'suit-tab suit-tab-task'
    readonly_fields = ('task',)
    created = ('task', 'task_status')

    def has_add_permission(self, request):
        return False


@admin.register(User)
class UserAdmin(UserAdmin, BaseModelAdmin):
    list_display = ('username', 'get_full_name', 'is_active')
    actions = (activate_action, deactivate_action)
    list_filter = ( 'groups', 'role', CTasksFilter)
    search_fields = ('=id', 'username', 'full_name', 'email')
    readonly_fields = ('last_login', 'date_joined', 'created', 'alt_id',
                       'email_verified_date')
    fieldsets = (
        (_('Персональная информация'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': ('username', 'last_name', 'first_name',
                       'gender', 'email', 'phone', 'city', 'user_description',
                       'social_instagram', 'social_vkontakte', 'photo'
                       )
        }),
        (_('Даты'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': ('born', 'last_login', 'created')
        }),
        (_('Служебная информация'), {
            'classes': ('suit-tab', 'suit-tab-extra'),
            'fields': ('role', 'token', 'alt_id', 'email_verified_date',
                       'password', 'is_staff', 'is_superuser', 'is_active',
                       'groups'
                       )
        }),
    )
    inlines = [WallInline, CourseTaskUserInline, CourseTaskMessageInline]
    formfield_overrides = {
        ThumbnailerImageField: {'widget': ImageClearableFileInput},
        models.TextField: {'widget': CKEditorWidget()}
    }
    suit_form_tabs = (
        ('general', _('Общее')),
        ('extra', _('Служебная информация')),
        ('wall', _('Лента пользователя')),
        ('task', _('Статусы заданий')),
        ('msgs', _('Сообщения')),
    )

    def get_actions(self, request):
        actions = super(UserAdmin, self).get_actions(request)
        if 'delete_selected' in actions and not request.user.is_superuser:
            del actions['delete_selected']
        return actions
