# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from snippets.models.abstract import RelatedPhoto


class BasicPhoto(RelatedPhoto):
    title = models.CharField(_('Текст'), max_length=555, null=True, blank=True)

    class Meta:
        verbose_name = _('Фотография')
        verbose_name_plural = _('Фотографии')
